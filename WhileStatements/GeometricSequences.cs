﻿namespace WhileStatements
{
    public static class GeometricSequences
    {
        public static uint SumGeometricSequenceTerms1(uint a, uint r, uint n)
        {
            // TODO 3-1. Implement the method that returns sum of a geometric sequence terms.
            throw new NotImplementedException();
        }

        public static uint SumGeometricSequenceTerms2(uint n)
        {
            // TODO 3-2. Implement the method that returns sum of a geometric sequence terms.
            throw new NotImplementedException();
        }

        public static uint CountGeometricSequenceTerms3(uint a, uint r, uint maxTerm)
        {
            // TODO 3-3. Implement the method that returns count of a geometric sequence terms.
            throw new NotImplementedException();
        }

        public static uint CountGeometricSequenceTerms4(uint a, uint r, uint n, uint minTerm)
        {
            // TODO 3-4. Implement the method that returns count of a geometric sequence terms.
            throw new NotImplementedException();
        }
    }
}
