﻿namespace WhileStatements
{
    public static class PrimeNumbers
    {
        public static bool IsPrimeNumber(uint n)
        {
            // TODO 5-1. Implement the method that returns true if n is a prime number; otherwise returns false.
            throw new NotImplementedException();
        }

        public static uint GetLastPrimeNumber(uint n)
        {
            // TODO 5-2. Implement the method that returns the latest prime number is (0, n] interval.
            throw new NotImplementedException();
        }

        public static uint SumLastPrimeNumbers(uint n, uint count)
        {
            // TODO 5-3. Implement the method that returns the sum of the latest _count_ prime numbers is (0, n] interval.
            throw new NotImplementedException();
        }
    }
}
